﻿namespace FolderMailToCSV
{
    partial class frmCSVMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.chkBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.StartDate = new System.Windows.Forms.DateTimePicker();
            this.EndDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnExtractCSV = new System.Windows.Forms.Button();
            this.chckBoxDate = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxFilepath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chkBoxSelectAll = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkBox});
            this.dataGridView1.Location = new System.Drawing.Point(12, 88);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(974, 319);
            this.dataGridView1.TabIndex = 2;
            // 
            // chkBox
            // 
            this.chkBox.HeaderText = "";
            this.chkBox.Name = "chkBox";
            // 
            // StartDate
            // 
            this.StartDate.Enabled = false;
            this.StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.StartDate.Location = new System.Drawing.Point(335, 11);
            this.StartDate.MinDate = new System.DateTime(2011, 1, 1, 0, 0, 0, 0);
            this.StartDate.Name = "StartDate";
            this.StartDate.Size = new System.Drawing.Size(200, 20);
            this.StartDate.TabIndex = 3;
            this.StartDate.Value = new System.DateTime(2019, 8, 1, 0, 0, 0, 0);
            // 
            // EndDate
            // 
            this.EndDate.Enabled = false;
            this.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.EndDate.Location = new System.Drawing.Point(625, 11);
            this.EndDate.MinDate = new System.DateTime(2011, 1, 1, 0, 0, 0, 0);
            this.EndDate.Name = "EndDate";
            this.EndDate.Size = new System.Drawing.Size(200, 20);
            this.EndDate.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(248, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Date de début: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(554, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Date de fin: ";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(248, 55);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 7;
            // 
            // btnExtractCSV
            // 
            this.btnExtractCSV.Enabled = false;
            this.btnExtractCSV.Location = new System.Drawing.Point(855, 10);
            this.btnExtractCSV.Name = "btnExtractCSV";
            this.btnExtractCSV.Size = new System.Drawing.Size(113, 23);
            this.btnExtractCSV.TabIndex = 8;
            this.btnExtractCSV.Text = "Extraire au CSV";
            this.btnExtractCSV.UseVisualStyleBackColor = true;
            this.btnExtractCSV.Click += new System.EventHandler(this.btnExtractCSV_Click);
            // 
            // chckBoxDate
            // 
            this.chckBoxDate.AutoSize = true;
            this.chckBoxDate.Location = new System.Drawing.Point(94, 15);
            this.chckBoxDate.Name = "chckBoxDate";
            this.chckBoxDate.Size = new System.Drawing.Size(93, 17);
            this.chckBoxDate.TabIndex = 10;
            this.chckBoxDate.Text = "Filtrer par date";
            this.chckBoxDate.UseVisualStyleBackColor = true;
            this.chckBoxDate.CheckedChanged += new System.EventHandler(this.chckBoxDate_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(436, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Filepath";
            // 
            // txtBoxFilepath
            // 
            this.txtBoxFilepath.Location = new System.Drawing.Point(497, 47);
            this.txtBoxFilepath.Name = "txtBoxFilepath";
            this.txtBoxFilepath.ReadOnly = true;
            this.txtBoxFilepath.Size = new System.Drawing.Size(343, 20);
            this.txtBoxFilepath.TabIndex = 12;
            this.txtBoxFilepath.TextChanged += new System.EventHandler(this.txtBoxFilepath_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(855, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Browse Folder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.browseFolder);
            // 
            // chkBoxSelectAll
            // 
            this.chkBoxSelectAll.AutoSize = true;
            this.chkBoxSelectAll.Location = new System.Drawing.Point(56, 65);
            this.chkBoxSelectAll.Name = "chkBoxSelectAll";
            this.chkBoxSelectAll.Size = new System.Drawing.Size(70, 17);
            this.chkBoxSelectAll.TabIndex = 14;
            this.chkBoxSelectAll.Text = "Select All";
            this.chkBoxSelectAll.UseVisualStyleBackColor = true;
            this.chkBoxSelectAll.CheckedChanged += new System.EventHandler(this.chkBoxSelectAll_CheckedChanged);
            // 
            // frmCSVMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 419);
            this.Controls.Add(this.chkBoxSelectAll);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtBoxFilepath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chckBoxDate);
            this.Controls.Add(this.btnExtractCSV);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.EndDate);
            this.Controls.Add(this.StartDate);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "frmCSVMail";
            this.Text = "Dtos-DownloadMailsToCSV";
            this.Load += new System.EventHandler(this.frmCSVMail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkBox;
        private System.Windows.Forms.DateTimePicker StartDate;
        private System.Windows.Forms.DateTimePicker EndDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnExtractCSV;
        private System.Windows.Forms.CheckBox chckBoxDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxFilepath;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkBoxSelectAll;
        //private System.Windows.Forms.DataGridViewTextBoxColumn SN;
    }
}

