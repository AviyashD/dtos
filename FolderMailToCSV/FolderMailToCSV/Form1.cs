﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Outlook.Application;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.IO;
using CsvHelper;
using System.Globalization;
using Exception = System.Exception;
using Microsoft.Exchange.WebServices.Data;
using System.Web.Services.Description;

namespace FolderMailToCSV
{
    //public static List<string> folderList = new List<string>();

    public partial class frmCSVMail : Form
    {

        public static List<MAPIFolder> _FolderList;
        public static string folderName = "";
        public frmCSVMail()
        {
            InitializeComponent();
        }


        private static void ReleaseComObject(object obj)
        {
            if (obj != null)
            {
                Marshal.ReleaseComObject(obj);
                obj = null;
            }
        }

        public class Mailclass
        {
            public string emailAddress { get; set; }
            public string subject { get; set; }
            public string body { get; set; }
            public string name { get; set; }
            public string date { get; set; }
        }


        private void btnListMail_Click(object sender, EventArgs e)
        {
            //ReadMailItems();
            ViewMail();

        }


        private string GetSenderEmailAddress(Outlook.MailItem mail)
        {
            try
            {
                Outlook.AddressEntry sender = mail.Sender;
                string SenderEmailAddress = "";
                if (sender != null && (sender.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeUserAddressEntry || sender.AddressEntryUserType == Outlook.OlAddressEntryUserType.olExchangeRemoteUserAddressEntry))
                {
                    Outlook.ExchangeUser exchUser = sender.GetExchangeUser();
                    if (exchUser != null)
                    {
                        SenderEmailAddress = exchUser.PrimarySmtpAddress;
                        //     otherDD = sender.PropertyAccessor.GetProperty(PrimarySmtpAddress);
                    }
                }
                else
                {
                    SenderEmailAddress = mail.SenderEmailAddress;
                }

                return SenderEmailAddress;
            }
            catch(Exception ex)
            {
                //  MessageBox.Show("GetSenderEmailAddress" + ex.Message);
                MessageBox.Show("Une erreur s'est produite!");

                return null;
            }

        }

        private void ProcessMailForCSV(MAPIFolder mailFolder)
        {
            try
            {
                lblStatus.Text = "Processing";
                List<Mailclass> mailList = new List<Mailclass>();
                string bodyContents = "";
                var sub = string.Empty;
                string recipientMail = "";
                string Name = "";
                string ReceivedTime = "";

                if (chckBoxDate.Checked==true)
                {
                    DateTime start = this.StartDate.Value.Date;
                    DateTime end = this.EndDate.Value.Date;
                    
                    // Initial restriction is Jet query for date range
                    string sFilter = "[ReceivedTime] >= '" +
                        start.ToString("g")
                        + "' AND [ReceivedTime] <= '" +
                        end.ToString("g") + "'";

                    foreach (object item in mailFolder.Items.Restrict(sFilter))
                    {
                        if (item is Outlook.MailItem)
                        {
                            Outlook.MailItem mailitem = (Outlook.MailItem)item;
                            recipientMail = GetSenderEmailAddress(mailitem);
                            // NameResolutionCollection coll = Service.ResolveName("/O=ABCD/OU=EXCHANGE ADMINISTRATIVE GROUP (ABCDEFGH)/CN=RECIPIENTS/CN=ABCD00000", ResolveNameSearchLocation.DirectoryOnly, true)

                            bodyContents = mailitem.Body != null ? mailitem.Body.Replace("\r\n", sub).Replace("\r", sub).Replace("\n", sub) : "";
                            ReceivedTime = mailitem.ReceivedTime.ToString("dd/MM/yyyy") != null ? mailitem.ReceivedTime.ToString("dd/MM/yyyy") : "";
                            Name = mailitem.SenderName != null ? mailitem.SenderName : "";
                            mailList.Add(new Mailclass
                            {
                                name = Name,
                                emailAddress = recipientMail,
                                date = ReceivedTime,
                                subject = mailitem.Subject,
                                body = bodyContents
                            });
                        }
                    }
                }else
                {
                    foreach (object item in mailFolder.Items)
                    {
                        if (item is Outlook.MailItem)
                        {
                            

                            Outlook.MailItem mailitem = (Outlook.MailItem)item;

                            recipientMail = GetSenderEmailAddress(mailitem);
                            bodyContents = mailitem.Body != null ? mailitem.Body.Replace("\r\n", sub).Replace("\r", sub).Replace("\n", sub) : "";
                            ReceivedTime = mailitem.ReceivedTime.ToString("dd/MM/yyyy") != null ? mailitem.ReceivedTime.ToString("dd/MM/yyyy") : "";
                            Name = mailitem.SenderName != null ? mailitem.SenderName : "";
                            mailList.Add(new Mailclass
                            {
                                name = Name,
                                emailAddress = recipientMail,
                                date=ReceivedTime,
                                subject = mailitem.Subject,
                                body = bodyContents
                            });
                        }

                    }
                }
                
                ToCSVFunction(mailList, mailFolder.Name);
                lblStatus.Text = "Completed";
            }
            catch(Exception ex)
            {
                lblStatus.Text = "";

                // MessageBox.Show("ProcessMailForCSV" + ex.Message);
                MessageBox.Show("Une erreur s'est produite!");
                
            }
            //finally
            //{
            //    ReleaseComObject(mailFolder);
            //}


        }

        public void ToCSVFunction(List<Mailclass> _mails, string fileName = "test1628170220")
        {
            try
            {
                string filepath = folderName+"\\" + fileName + ".csv";
                using (var writer = new StreamWriter(filepath, false, Encoding.UTF8))
                using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csvWriter.Configuration.Delimiter = ",";
                    csvWriter.WriteField("Name");
                    csvWriter.WriteField("Email");
                    csvWriter.WriteField("Date");
                    csvWriter.WriteField("Sujet");
                    csvWriter.WriteField("Contenu");
                    csvWriter.NextRecord();

                    foreach (var mail in _mails)
                    {
                        csvWriter.WriteField(mail.name);
                        csvWriter.WriteField(mail.emailAddress);
                        csvWriter.WriteField(mail.date);
                        csvWriter.WriteField(mail.subject);
                        csvWriter.WriteField(mail.body);
                        csvWriter.NextRecord();
                    }

                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show("ToCSVFunction" + ex.Message);
                MessageBox.Show("Une erreur s'est produite!");

            }


        }

        public List<MAPIFolder> GetFolders(MAPIFolder folder, List<MAPIFolder> folderList)
        {
            try
            {
                if (folder.Folders.Count == 0)
                {

                    folderList.Add(folder);
                    Items mailItems = null;
                    mailItems = folder.Items;
                    return folderList;
                }
                else
                {
                    foreach (MAPIFolder subFolder in folder.Folders)
                    {
                        GetFolders(subFolder, folderList);
                    }
                }
                return folderList;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Une erreur s'est produite!");

            }
            return folderList;
        }


        public void ViewMail()
        {
            try
            {
                Application outlookApplication = null;
                NameSpace olNS = null;
                outlookApplication = new Application();
                olNS = outlookApplication.GetNamespace("MAPI");
                Outlook.MAPIFolder oPublicFolder = olNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolder‌​Inbox).Parent;
               // Outlook.MAPIFolder oPublicFolder = olNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolder‌​Inbox);

                List<MAPIFolder> folderList = new List<MAPIFolder>();

                folderList = GetFolders(oPublicFolder, folderList);
                _FolderList = folderList;
                string path = "";


                dataGridView1.DataSource = (folderList);
                //this.dataGridView1.Columns["FullFolderPath"].Visible = false;
                this.dataGridView1.Columns["Items"].Visible = false;
                this.dataGridView1.Columns["Parent"].Visible = false;
                this.dataGridView1.Columns["Folders"].Visible = false;             
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dataGridView1.Columns["Name"].ReadOnly = true;
                dataGridView1.Columns["FolderPath"].ReadOnly = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Une erreur s'est produite!");

            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                 
                        DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                        if (Convert.ToBoolean(row.Cells["chkBox"].Value))
                        {
                            MessageBox.Show("Selected ID: " + row.Cells["Name"].Value);
                        }

                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur s'est produite!");

            }
        }

        private void frmCSVMail_Load(object sender, EventArgs e)
        {
            ViewMail();
        }

        private string BrowseFile()
        {
          try
            { 
            string _folderName = "";
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Show the FolderBrowserDialog.  
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                _folderName = folderDlg.SelectedPath;


            }
            return _folderName;
             }
               catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("Une erreur s'est produite!");
                return "";
            }
        }
        private void btnExtractCSV_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> selectedFoldersLst = new List<string>();
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells["chkBox"].Value != null)
                    {
                        if ((bool)dataGridView1.Rows[i].Cells["chkBox"].Value)
                        {
                            selectedFoldersLst.Add(dataGridView1.Rows[i].Cells["FolderPath"].Value.ToString());
                        }
                    }

                }

                if (selectedFoldersLst.Count != 0)
                {
                    // browseFile();
                    if (folderName!=null)
                    {

                    foreach (string item in selectedFoldersLst)
                    {                       

                        foreach (MAPIFolder folder in _FolderList)
                        {
                            if (folder.FolderPath.ToString().Contains(item))
                            {
                                ProcessMailForCSV(folder);
                                break;
                            }
                        }
                    }
                    }
                    else MessageBox.Show("Aucun dossier n'est choisi!", "Statut dossier", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Aucun fichier n'est choisi!", "Statut dossier", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message);
                MessageBox.Show("Une erreur s'est produite!");

            }

        }

        private void browseFolder(object sender, EventArgs e)
        {
            try
            {
                //string physicalPath = Server.MapPath("~/Content/albums");
                //string path = Path.Combine(physicalPath, album.title.Replace(" ", ""));

                folderName = BrowseFile();
            txtBoxFilepath.Text = folderName;
            }
            
           catch (Exception ex)
            {
                MessageBox.Show("Une erreur s'est produite!");

            }
        }

        private void chckBoxDate_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
            if(chckBoxDate.Checked)
            {
                StartDate.Enabled = true;
                EndDate.Enabled = true;
            }
            else
            {
                StartDate.Enabled = false;
                EndDate.Enabled = false;
            }
            }

           

                  catch (Exception ex)
            {
                MessageBox.Show("Une erreur s'est produite!");

            }

        }

        private void txtBoxFilepath_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtBoxFilepath.Text == "")
                    btnExtractCSV.Enabled = false;
                else
                    btnExtractCSV.Enabled = true;
            }
            catch (Exception ex)            
           
            {
                MessageBox.Show("Une erreur s'est produite!");

            }
        }

        private void chkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if(chkBoxSelectAll.Checked)
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Cells["chkBox"].Value = true;

                }

            }
            else
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    dataGridView1.Rows[i].Cells["chkBox"].Value = false;

                }
            }


        }
    }
}
